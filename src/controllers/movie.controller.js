const database = require('../database/database')
const logger = require('../config/appconfig').logger

module.exports = {
  getAllMovies: (req, res) => {
    logger.info('Get /api/movies aangeroepen')
    res.status(200).json({ result: database.movies })
  },

  getMovieById: function(req, res, next) {
    console.log('Get /api/movies/id aangeroepen')
    const id = req.params.movieId
    if (id >= 0 && id < database.movies.length) {
      res.status(200).json({ result: database.movies[id] })
    } else {
      const errorObject = {
        message: 'Movie was not found',
        code: 500
      }
      next(errorObject)
    }
  },

  createMovie: function(req, res, next) {
    logger.info('Post /api/movies aangeroepen')
    // hier komt in het request een movie binnen.
    const movie = req.body
    logger.info(movie)
    database.addMovie(movie, function(err, result) {
      if (err) {
        next(err)
      }
      if (result) {
        movie['id'] = result.id
        logger.info('Done, sending response')
        res.status(200).json({ result: movie })
      }
    })
  }
}
