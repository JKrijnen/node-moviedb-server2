const logger = require("../config/appconfig").logger;

const database = {
  movies: [],
  users: [],
  favorites: [],

  addMovie: function(movie, callback) {
    logger.info("Adding movie to database");
    setTimeout(() => {
      if (database.movies.length < 3) {
        database.movies.push(movie);
        logger.info("Added movie");
        // Success: return id van toegevoegde movie.
        callback(null, { id: database.movies.length - 1 });
      } else {
        logger.error("Database is full!");
        const errorObject = {
          message: "Database is full!",
          code: 500
        };
        callback(errorObject, null);
      }
    }, 300);
  }
};

module.exports = database;
